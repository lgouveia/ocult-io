#include <dlfcn.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>
#include <string>

// Function pointer type for the original 'open'
typedef int (*original_open_type)(const char *pathname, int flags, ...);

// Override the 'open' function
extern "C" int open(const char *pathname, int flags, ...) {
    // Get the pointer to the original 'open' function
    original_open_type original_open;
    original_open = (original_open_type)dlsym(RTLD_NEXT, "open");

    // Handle variadic arguments for mode
    va_list args;
    va_start(args, flags);
    mode_t mode = va_arg(args, mode_t);
    va_end(args);

    // Redirect logic
    std::string new_path = "/tmp/";
    new_path += pathname;

    // Call the original open function with the new path and mode
    return original_open(new_path.c_str(), flags, mode);
}

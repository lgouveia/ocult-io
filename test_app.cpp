#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <ctime>

int main() {
    const char* filename = "test_file.txt";
    int fd = open(filename, O_CREAT | O_WRONLY, 0644);

    if (fd == -1) {
        std::cerr << "Failed to create file: " << filename << std::endl;
        return 1;
    }

    // Write random content to the file
    srand(time(nullptr));
    char buffer[100];
    for (int i = 0; i < 10; ++i) {
        int len = snprintf(buffer, sizeof(buffer), "Random Number: %d\n", rand() % 100);
        write(fd, buffer, len);
    }
    close(fd);

    // Read and display the content from the file
    fd = open(filename, O_RDONLY);
    if (fd == -1) {
        std::cerr << "Failed to open file: " << filename << std::endl;
        return 1;
    }

    while (int len = read(fd, buffer, sizeof(buffer) - 1)) {
        buffer[len] = '\0';
        std::cout << buffer;
    }
    close(fd);

    return 0;
}

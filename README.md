# Ocult-IO

Ocult-IO is an I/O library that transparently redirects I/O operations. It was created to optimize the I/O of workflows by avoiding sending files to shared file systems. Instead, files are written locally (on an SSD, for example), so the next task in the workflow can read them directly from the local disk.

Everything is made in a transparent way, meaning that NOTHING in the application needs to be modified.

# Getting Started

## Requirements

- G++ compiler 9.4 or higher

## Installation

Compile the library using g++:

```bash
g++ -fPIC -shared -o libfile_redirect.so file_redirect.cpp -ldl    
```

Compile the example file:

```bash
g++ -o test_app test_app.cpp   
```

## Testing the Library

By default, the library will redirect the file created by `test_app` to the folder `/tmp/`. To verify if the library is working, we can first execute the test_app without the library:

```bash
❯ ./test_app
Random Number: 16
...
```

This will generate the file test_file.txt with a list of random numbers.

Then, we can execute `test_app` using the library:

```bash
LD_PRELOAD=/fullpath/to/libfile_redirect.so ./test_app
Random Number: 62
...
```

Note: it is necessary to provide the full path to the library when using the LD_PRELOAD variable.

By examining the files, we can see that the one created using libfile_redirect.so was created in the /tmp/ directory, and that the first file created in the current directory was not altered:

```bash
❯ cat /tmp/test_file.txt
Random Number: 62
...
❯ cat test_file.txt
Random Number: 16
...
```
